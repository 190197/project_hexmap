using UnityEngine;
using UnityEngine.UI;

namespace Hermanus {
    /* Summary
        This script is resposible for displaying the noise texutures
        Debugging only.

     */

    public class NoiseTexturesDisplay : MonoBehaviour {

        [Header ("Terrain Height")]
        [SerializeField] RawImage terrainHeight;

        [Header ("Terrain Type")]
        [SerializeField] RawImage terrainType;

        public void DisplayTerrainHeight (Texture2D _terrainHeight) {
            terrainHeight.texture = _terrainHeight;
        }
        public void DisplayTerrainType (Texture2D _terrainType) {
            terrainType.texture = _terrainType;
        }

    }
}
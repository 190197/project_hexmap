﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Hermanus {

    public class ObjectPool<T> : MonoBehaviour where T : MonoBehaviour {

        [SerializeField] public GameObject opPrefab;
        [SerializeField] bool singleton = true;
        Queue<T> pool = new Queue<T> ();
        public static ObjectPool<T> Instance { get; private set; }

        protected internal class TEvent : UnityEvent<T> { }
        protected internal TEvent OnGetFromPool = new TEvent ();

        protected virtual void Awake () {
            if (singleton) Instance = this;
        }

        public T GetFromObjectPool () {
            T opObject = default (T);

            if (pool.Count > 0) {
                opObject = pool.Dequeue ();
            } else {
                opObject = Instantiate (opPrefab, transform).GetComponent<T> ();
            }
            opObject.gameObject.SetActive (true);

            OnGetFromPool.Invoke (opObject);

            return opObject;
        }

        public void PutIntoObjectPool (T opObject) {
            pool.Enqueue (opObject);
            opObject.transform.SetParent (transform, false);
            opObject.gameObject.SetActive (false);
        }

    }

}
using UnityEngine;
namespace Hermanus {
    /* Summary HexCell 
        Hex Cell Carries all the cells Data
            ---Hex Cell contains his coordinates
            ---Hex Cell contains there neighbors
            ---Hex Cell contains Terrain Type
    */
    public class HexCell : MonoBehaviour {
        public HexCoordinates coordinates;
        public int elevation;
        [SerializeField] HexCell[] neighbors;
        [SerializeField] HexTypeSO currentTerrainType;

        public HexCell GetNeigbor (HexDirection direction) {
            return neighbors[(int) direction];
        }

        public void SetNeighbour (HexDirection direction, HexCell cell) {
            neighbors[(int) direction] = cell;
            cell.neighbors[(int) direction.Opposite ()] = this;
        }

        public void SetElevation (int _elevation) {
            elevation = _elevation;
        }

        public void SetTerrainType (HexTypeSO _typesOfTerrainSOs) {
            currentTerrainType = _typesOfTerrainSOs;
        }

    }
}
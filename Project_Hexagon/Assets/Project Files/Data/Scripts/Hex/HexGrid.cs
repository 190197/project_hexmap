using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyButtons;

namespace Hermanus {
    /* Summary
        The Hex Grid Generates a grid based of the HexMetrics 
            The grid gets populated through a double for loop with a method called CreatCell
            Create cell takes 3 peramaters w(map width) h(map height) i(an indexer)
            Create cell uses a noice map an determins a random elevavation for the map
            Create cell gets and sets the neightbors 
            Create cell sets the coordintes
            Create cell poplulates the filled ground
     */
    public class HexGrid : MonoBehaviour {

        [Header ("Terrain Size")]
        [SerializeField] Vector2Int terrainSize; // Size of the map x = width, y = height
        [SerializeField] float maxHeight = 5f;

        [SerializeField] List<HexTypeSO> amountOfTerrainTypes = new List<HexTypeSO> ();
        [Header ("Object Pools")]
        [SerializeField] ObjectPoolHex opHexTerrain;
        [SerializeField] ObjectPoolFill opFillHex;

        [Space]
        [Header ("NoiseGenerators")]
        [SerializeField] TerrainHeightNoise heightNoise;
        [SerializeField] TerrainNoise terrainNoise;

        [Space]

        [Header ("Debugging")]
        [SerializeField] bool debugging;
        [SerializeField] NoiseTexturesDisplay noiseTexturesDisplay;

       [SerializeField] List<HexCell> cells = new List<HexCell> ();
        List<FillCell> filledGround = new List<FillCell> ();
        Texture2D terrainHeightTexture;
        Texture2D terrainTypeTexture;

        /* 
            Button Press
        */
        [Button]
        public void GenerateTerrain () {
            ClearFillCell ();
            ClearHexCell ();
            terrainHeightTexture = heightNoise.GenerateNoiceTexture ();
            terrainTypeTexture = terrainNoise.GenerateNoiceTexture ();

            if (debugging && noiseTexturesDisplay != null) {
                noiseTexturesDisplay.DisplayTerrainHeight (terrainHeightTexture);
                noiseTexturesDisplay.DisplayTerrainType (terrainTypeTexture);
            }

            for (int h = 0, i = 0; h < terrainSize.y; h++) {
                for (int w = 0; w < terrainSize.x; w++) {
                    CreateCell (w, h, i++);
                }

            }

        }

        void CreateCell (int w, int h, int i) { // Can change this to a quarotine 

            Vector3 position;

            int xCoordH = Mathf.FloorToInt (((float) w * (float) heightNoise.textureSize.x / (float) terrainSize.x));
            int yCoordH = Mathf.FloorToInt (((float) h * (float) heightNoise.textureSize.y / (float) terrainSize.y));

            int xCoordT = Mathf.FloorToInt (((float) w * (float) terrainNoise.textureSize.x / (float) terrainSize.x));
            int yCoordT = Mathf.FloorToInt (((float) h * (float) terrainNoise.textureSize.y / (float) terrainSize.y));

            float pixelValueH = terrainHeightTexture.GetPixel (xCoordH, yCoordH).r;
            float pixelValueT = terrainTypeTexture.GetPixel (xCoordT, yCoordT).r;

            float hexHeight = pixelValueH * maxHeight;
            int terrainIndex = (int) Mathf.Round (pixelValueT * (amountOfTerrainTypes.Count - 1));

            hexHeight = Mathf.Round (hexHeight);

            position.x = (w + h * 0.5f - h / 2) * (HexMetrics.innerRadius * 2f); // For hex of set devide height by 2 not 2f
            position.y = hexHeight;

            position.z = h * (HexMetrics.outerRadius * 1.5f);
            SetHexCell (terrainIndex);

            HexCell cell = Instantiate<HexCell>(amountOfTerrainTypes[terrainIndex].hexTypePrefab.GetComponent<HexCell>());
            cell.SetTerrainType (amountOfTerrainTypes[terrainIndex]);
            cells.Add (cell);
            cell.transform.SetParent (transform, false);
            cell.transform.localPosition = position;
            cell.coordinates = HexCoordinates.FromOffsetCoordinates (w, h);

            cell.SetElevation ((int) hexHeight);

            if (w > 0) {
                cell.SetNeighbour (HexDirection.W, cells[i - 1]);
            }
            if (h > 0) {
                if ((h & 1) == 0) {
                    cell.SetNeighbour (HexDirection.SE, cells[i - terrainSize.x]);
                    if (w > 0) {
                        cell.SetNeighbour (HexDirection.SW, cells[i - terrainSize.x - 1]);
                    }
                } else {
                    cell.SetNeighbour (HexDirection.SW, cells[i - terrainSize.x]);
                    if (w < terrainSize.x - 1) {
                        cell.SetNeighbour (HexDirection.SE, cells[i - terrainSize.x + 1]);
                    }
                }
            }
            StartCoroutine (FillTerrain (i));

        }

        void SetHexCell (int terrain) {
            opHexTerrain.opPrefab = amountOfTerrainTypes[terrain].hexTypePrefab;
            Debug.Log(opHexTerrain.opPrefab.name);
        }

        IEnumerator SetTerrainsType () {
            yield return null;
        }
        IEnumerator FillTerrain (int i) {

            Vector3 position;
            int currentElevationToFill = cells[i].elevation;
            position.x = cells[i].transform.position.x;
            position.y = cells[i].transform.position.y - 0.5f;
            position.z = cells[i].transform.position.z;
            // Each hex is .5 m heigh if pos is 1 it means 2 fills needs to spawn
            for (var e = 0; e < (currentElevationToFill * 2) - 1; e++) {
                FillCell fillCell = opFillHex.GetFromObjectPool ();
                filledGround.Add (fillCell);
                fillCell.transform.localPosition = position;
                position.y -= 0.5f;
            }
            yield return null;

        }
        void ClearHexCell () {
            if (cells.Count > 0) {
                for (var i = cells.Count - 1; i >= 0; i--) {
                    opHexTerrain.PutIntoObjectPool (cells[i]);
                    cells.RemoveAt (i);
                }
            }

        }
        void ClearFillCell () {
            if (filledGround.Count > 0) {
                for (var i = filledGround.Count - 1; i >= 0; i--) {
                    opFillHex.PutIntoObjectPool (filledGround[i]);
                    filledGround.RemoveAt (i);
                }
            }

        }

    }
}
using UnityEngine;

namespace Hermanus {
    /* Summary Hex Metrics 
        This is where I define the 6 vertexis for the Hexagon.
    */
    public static class HexMetrics {
        public const float outerRadius = 1f; // This is the outer radius for your hex shape.
        public const float innerRadius = outerRadius * 0.866025404f; // InnerRadius is calculated: ir = r x 3sqr/2

        public static Vector3[] corners = {
            new Vector3 (0f, 0f, outerRadius),
            new Vector3 (innerRadius, 0f, 0.5f * outerRadius),   // 0.5f is Half the length of a side
            new Vector3 (innerRadius, 0f, -0.5f * outerRadius),
            new Vector3 (0f, 0f, -outerRadius),
            new Vector3 (-innerRadius, 0f, -0.5f * outerRadius),
            new Vector3 (-innerRadius, 0f, 0.5f * outerRadius),
            new Vector3 (0f, 0f, outerRadius)

        };

    }
}
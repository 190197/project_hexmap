using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Hermanus {
    /* Summary
        Creates perlin Noise based of a height and withd
     */
    public class NoiseGenerator : MonoBehaviour {
        [Header (" Terrain Height")]
        [SerializeField] int seed;
        [SerializeField] bool randomiseSeed;
        [SerializeField] public Vector2Int textureSize;
        [Range (1f, 20f), SerializeField] float noiseScale = 10f;

        public Texture2D GenerateNoiceTexture () {
            Texture2D texture = new Texture2D (textureSize.x, textureSize.y);
            float pixelValue = 0;

            System.Random rng = new System.Random (randomiseSeed ? Random.Range (0, 999999) : seed);
            float offset = (float) rng.NextDouble () * rng.Next (100);

            for (var h = 0; h < textureSize.y; h++) {
                for (var w = 0; w < textureSize.x; w++) {

                    float xCoord = offset + ((float) w / (float) textureSize.x) * noiseScale;
                    float yCoord = offset + ((float) h / (float) textureSize.y) * noiseScale;

                    pixelValue = Mathf.PerlinNoise (xCoord, yCoord);

                    texture.SetPixel (w, h, new Color (pixelValue, pixelValue, pixelValue));
                }
            }

            texture.Apply ();
            return texture;
        }

    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hermanus {
    /* Summary
        Scriptable Object to determin diffrent hex terrains.
    */

    [CreateAssetMenu ()]
    public class HexTypeSO : ScriptableObject {

        [SerializeField] public GameObject hexTypePrefab;
    }
}
﻿ # Welcome to HexGen World
**Version 1.0**
**Made with Unity version 2020.3.5f1**
## About 

HexGen World is a procedural hex map genaration tool. The purpose of the tool is to create a tool that automates/enables the creation of a wealth of content that would otherwise be unfeasible by hand. The content created by the tool should feel as close to handmade as possible and without feeling chaotic and random and was intended for my 3rd Year subject.

This tool allows the user to create their own hex tiles for video games such as Civilizations.

A variety of noise map are generated to create biome's such as forest, plains, hills etc. Hills and rivers will be generated as a result of elevation. Each tile can hold its own data that could as an example influence movement or be a different resource.  This tool is aimed to give the Designer the ability to implement the infuencial data themselfs.

## Process

The user will create their assets using a 3D modeling software.

**Considerations for the model**

*Currently the tool is still underdevelopment and as result the created assets needs to follow predeterminded specifications.*

 - The outer radius for the hex needs to be 1m (meters).
 - The height of the hex needs to be 0.5m (meters).

After the considerations the user can make the biomes the want to use. With this its recomended to make a fill/ground hex as well.

The user can the add as many biomes that the want to use to generate their enviroment. The enviroment is generated using Perlin noise to calculate the height as well as to calcultate the diffrent terrain types.

## How to use
[Youtube Demo Video](https://youtu.be/Z-ZJ_UbgSLE)
## Packages
Packages that this tool use that is included in the git are:

 - EasyButtons

## Future Plans

Sofar the future development plans are:

 - Able to use diffrent noise types
 - Make the tool more user friendly
 - Add checks for watermasses
 - Make the assets that can be implemented more flexible


## Developer Info
Report any bugs to:

hermanusamarais@gmail.com
